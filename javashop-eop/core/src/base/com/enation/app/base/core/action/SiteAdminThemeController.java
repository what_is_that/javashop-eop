package com.enation.app.base.core.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.enation.eop.resource.IAdminThemeManager;
import com.enation.eop.resource.ISiteManager;
import com.enation.eop.resource.model.EopSite;

/**
 * 站点主题管理
 * @author gongqin
 */
@Scope("prototype")
@Controller
@RequestMapping("/core/admin/user")
public class SiteAdminThemeController {
	
	@Autowired
	private IAdminThemeManager adminThemeManager;
	@Autowired
	private ISiteManager siteManager;
	

	@RequestMapping(value = "/siteAdminTheme")
	public ModelAndView editInput() {
//		String contextPath = this.getRequest().getContextPath();
//		previewBasePath =  contextPath+ "/adminthemes/";
//		adminTheme = adminThemeManager.get( EopSite.getInstance().getAdminthemeid());
//		listTheme = adminThemeManager.list();
//		previewpath = previewBasePath + adminTheme.getPath() + "/preview.png";
//		return SUCCESS;
//		
		
		ModelAndView view = new ModelAndView();
		view.setViewName("/core/admin/theme/file_list");

		return view;
	}

}
